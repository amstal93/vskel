import argparse, subprocess

parser = argparse.ArgumentParser(description='For those too lazy to type docker-compose in the shell.')

parser.add_argument(
    '-c', '--command',
    help='what to do with docker-compose',
    choices=['build', 'up', 'down'],
    type=str,
    default='up'
)

parser.add_argument(
    '-p', '--production',
    help='mode to spin docker up in',
    action="store_true",
    default=False
)


args = parser.parse_args()

command = ['docker-compose']

command.append('-f')
command.append('docker-compose.web.production.yml')
if args.production == False:
    command.append('-f')
    command.append('docker-compose.web.development.yml')


command.append(args.command)
subprocess.run(command)
