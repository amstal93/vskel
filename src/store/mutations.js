import Vue from 'vue'
import Vuex from 'vuex'

const mutations = {
  // yes this is not the best convention
  set(state, {k, v}) { Vue.set(state, k, v) },
  sset(state, {k, v, s}) { Vue.set(state[k], s, v) },
}


export default mutations
